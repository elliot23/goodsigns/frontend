export default {
  theme: {
    dark: false,
    options: {
      customProperties: true,
      // minifyTheme: css => (process.env.NODE_ENV === 'production' ? css.replace(/[\r\n|\r|\n]/g, '') : css)
    },

    themes: {
      light: {},
      dark: {},
    },
  },
  /* lang: {
    locales: { ru },
    current: 'ru',
  }, */
}
