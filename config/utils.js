import { resolve } from 'path'
import getenv from 'getenv'

export const rootDir = process.cwd()
export const srcDir = resolve(rootDir, 'src')
export const isDev = getenv('NODE_ENV') !== 'production'
export const baseUrl = getenv('BASE_URL', '/')
export const target = getenv('APP_TARGET')
export const modern = getenv.bool('APP_MODERN') && !isDev
export const ssr = getenv.bool('APP_SSR')
export const components = getenv.bool('APP_COMPONENTS')
export const loading = getenv('APP_LOADING_COMPONENT') || JSON.parse(getenv('APP_LOADING_OPTIONS'))
export const locales = JSON.parse(getenv('LOCALES'))
export const css = [...JSON.parse(getenv('STYLES_LIST')), ...[getenv('STYLES_APP')]]
export const styleResources = JSON.parse(getenv('STYLES_RES'))
export const googleFonts = JSON.parse(getenv('GOOGLE_FONTS'))
export const telemetry = getenv.bool('APP_TELEMETRY')
