import getenv from 'getenv'

export default {
  /* bundleRenderer: {
    directives: {
      scrollReveal(node) {
        const cssClass = node.data.class || (node.data.class = {})
        if (Array.isArray(cssClass)) {
          cssClass.push('opacity-0')
        } else {
          cssClass.class = 'opacity-0'
        }
      },
    },
  }, */
  /* etag: {
    // import { murmurHash128 } from 'murmurhash-native'
    hash: html => murmurHash128(html),
  }, */

  ssrLog: getenv('SSR_LOG'),
}
