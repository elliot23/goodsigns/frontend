const { APP_NAME, APP_LOCALE, YANDEX_VERIFICATION } = process.env

export default {
  title: APP_NAME,
  htmlAttrs: {
    lang: APP_LOCALE,
    dir: ['fa', 'ar', 'he'].includes(APP_LOCALE) ? 'rtl' : 'ltr',
  },
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    // { hid: 'og:site_name', property: 'og:site_name', content: APP_NAME },
    { hid: 'og:type', property: 'og:type', content: 'website' },
    { hid: 'description', name: 'description', content: '' },
    { name: 'format-detection', content: 'telephone=no' },
    { name: 'yandex-verification', content: YANDEX_VERIFICATION },
  ],
  link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  noscript: [{ innerHTML: 'This website requires JavaScript.' }],
}
