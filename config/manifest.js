const { APP_NAME } = process.env

export default {
  name: APP_NAME,
  short_name: APP_NAME,
  start_url: '/?utm_source=homescreen',
  display: 'standalone',
  lang: 'ru',
  icons: [
    {
      src: 'icon.png',
      type: 'image/png',
      sizes: '192x192',
    },
  ],
  background_color: '#ffffff',
  theme_color: '#ffffff',
}
