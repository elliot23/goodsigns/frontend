// import { isDev } from './utils'

export default {
  extractCSS: true,
  /* extractCSS: !isDev,
  cssSourceMap: isDev,
  devtools: isDev,
  parallel: isDev,
  cache: isDev,
  hardSource: isDev, */

  /* loaders: {
    vueStyle: { manualInject: true },
  }, */

  transpile: [/^vuetify/],

  profile: {
    color: '#e74c36',
  },

  extend(config, { isDev, isClient }) {
    if (isDev) {
      config.devtool = isClient ? 'source-map' : 'inline-source-map'

      if (isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    }
  },
}
