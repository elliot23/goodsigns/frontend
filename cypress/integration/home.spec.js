describe('The Home Page', () => {
  /* beforeEach(() => {
    cy.visit('/')
  }) */

  it('test', () => {
    cy.visit('/')
    cy.get('a[data-cy="main-menu__catalog"]').click()
    cy.get('a[data-cy="main-menu__contact-us"]').click()
    cy.get('a[data-cy="main-menu__service"]').click()
    cy.get('a[data-cy="home-link"]').click()
  })
})