#!/usr/bin/env node

const semver = require('semver')
const { engines } = require('../package')

const version = engines.node
if (!semver.satisfies(process.version, version)) {
  console.log(`Required node version ${version} not satisfied with current version ${process.version}.`)
  // eslint-disable-next-line
  process.exit(1)
}
