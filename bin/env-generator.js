#!/usr/bin/env node

const fs = require('fs')
const { resolve } = require('path')
const crypto = require('crypto')
const yargs = require('yargs')
const chalk = require('chalk')
const chalkTable = require('chalk-table')

const { argv } = yargs
  .option('mode', {
    alias: 'm',
    description: 'Environment mode',
    type: 'string',
    default: '',
  })
  .option('overwrite', {
    alias: 'o',
    description: 'Overwrite an existing file',
    type: 'boolean',
    default: true,
  })
  .option('quiet', {
    alias: 'q',
    description: 'Quiet mode does not output anything to the console',
    type: 'boolean',
  })
  .help()
  .alias('help', 'h')

const a = new Uint32Array(2)

const { mode } = argv
const isDev = mode === 'development'
const fileName = ['.env', mode].filter(i => i).join('.')
const rootPath = resolve(__dirname, '..')
const destPath = resolve(rootPath, fileName)
const jsonPath = resolve(rootPath, 'env.json')
const jsonBody = {
  main: {},
  development: {},
  production: {
    HOST: '0.0.0.0',
  },
}
const destExists = fs.existsSync(destPath)

const APP_KEY = Buffer.from(crypto.randomFillSync(a).buffer, a.byteOffset, a.byteLength).toString('hex')
const DEBUG = isDev ? 'nuxt:*' : false

const defaultValues = {
  DEBUG,
  NUXT_DEBUG: DEBUG,

  // --- PACKAGE MANAGERS
  // NPM_CONFIG_PRODUCTION: !isDev,
  // YARN_PRODUCTION: !isDev,
  // NODE_ENV: mode,

  // --- APP ---
  APP_KEY,
  APP_NAME: 'app',
  APP_HOST: 'localhost',
  APP_TARGET: 'server',
  APP_MODERN: false,
  APP_SSR: true,
  APP_COMPONENTS: true,
  APP_LOADING_OPTIONS: { color: 'yellowgreen', height: '5px' },
  APP_LOADING_COMPONENT: '', // '~/components/loader',
  APP_TELEMETRY: false,
  APP_LOCALE: 'ru',

  // --- CACHE & SESSION ---
  CACHE_VIEWS: false,
  SESSION_DRIVER: 'cookie',

  // --- DATABASE ---
  DB_CONNECTION: 'mysql',
  DB_HOST: 'localhost',
  DB_USER: 'root',
  DB_PASSWORD: '',
  DB_DATABASE: 'dev',

  // --- API ---
  API_URL: 'http://localhost:3003',
  // API_USER: 'root',
  // API_PASSWORD: '',

  // --- AXIOS ---
  AXIOS_DEBUG: false,
  AXIOS_PROXY: false,
  AXIOS_HTTPS: false,

  // --- APOLLO ---
  APOLLO_ENDPOINT: 'http://localhost:3004',
  APOLLO_TOKEN: null,

  // --- MAIN COMPONENTS ---
  MAIN_COMPONENTS_VISIBLE: true,

  // --- LOCALES ---
  LOCALES: {
    ru: 'ru-RU',
    // en: 'en-US',
  },

  // --- RENDER ---
  SSR_LOG: isDev,

  // --- NAVIGATION ---
  MENU_KEYS: {
    main: 'main_menu',
    right: 'right_menu',
    hidden: 'hidden_menu',
  },

  // --- COCKPIT ---
  COCKPIT_LAYOUT: {
    siteName: 'site_name',
    mainMenu: 'main_menu',
    hiddenMenu: 'hidden_menu',
  },

  // --- CATALOG ---
  CATALOG_BASE_URL: '/catalog',

  // --- STYLES ---
  STYLES_LIST: [],
  STYLES_APP: '~/assets/styles/app.scss',
  STYLES_RES: {
    scss: '~/assets/styles/_global.scss',
  },

  // -- FONTS ---
  GOOGLE_FONTS: [],

  // --- VUETIFY ---
  VUETIFY_FONT: 'Roboto',
  VUETIFY_ICONFONT: 'mdi',
  VUETIFY_ICONS: {},
  VUETIFY_THEMES: {},

  // --- RECAPTCHA ---
  RECAPTCHA_SITE_KEY: '',

  // --- YANDEX ---
  YANDEX_MAPS_API: '',
  YANDEX_VERIFICATION: '',
  YANDEX_METRIKA_ID: '',
}

const extraValues = fs.existsSync(jsonPath) ? JSON.parse(fs.readFileSync(jsonPath, 'utf8')) : {}
const resultValues = {
  ...defaultValues,
  ...(extraValues.main || {}),
  ...(extraValues[mode] || {}),
}

const fileBody = Object.entries(resultValues)
  .map(i => `${i[0]}=${typeof i[1] === 'boolean' ? i[1] : JSON.stringify(i[1])}`)
  .join('\n')

if (!argv.quiet) {
  if (!Object.keys(extraValues).length) {
    console.log(chalk.bgYellow('[Info]: no extra values were found'))
  }

  console.log(chalk.bgMagenta(` --- ENV-GENERATOR [${fileName}] ---`))
  console.log(
    chalkTable(
      {
        leftPad: 2,
        columns: [
          { field: 'key', name: chalk.bgBlueBright(' KEY ') },
          { field: 'value', name: chalk.bgMagentaBright(' VALUE ') },
        ],
      },
      Object.entries(resultValues).map(e => ({
        key: chalk.blue(e[0]),
        value: chalk.white(JSON.stringify(e[1])),
      }))
    )
  )
}

if (!destExists || argv.overwrite) fs.writeFileSync(destPath, fileBody)
else console.log(chalk.bgYellow('[Info]: The file cannot be overwritten (argv.overwrite)'))

if (!fs.existsSync(jsonPath)) fs.writeFileSync(jsonPath, JSON.stringify(jsonBody))
