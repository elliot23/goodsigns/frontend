#!/usr/bin/env node

const fs = require('fs')
const { resolve } = require('path')
const axios = require('axios')

process.env.NODE_ENV = 'production'

require('dotenv-flow').config()

const { API_URL } = process.env
const destPath = resolve(process.cwd(), 'routes.json')
const urls = ['cockpit', 'oasis'].map(route => axios.get(`${API_URL}/v1/${route}/routes`))

async function generate() {
  const result = await axios.all(urls)
  const routes = ['/', ...result.reduce((acc, cur) => [...acc, ...cur.data], [])]

  fs.writeFileSync(destPath, JSON.stringify(routes))

  console.log('Done.')
}

generate()
