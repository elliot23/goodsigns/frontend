import { resolve } from 'path'
import chalk from 'chalk'
import getenv from 'getenv'

const env = require('dotenv-flow').config()
const { build, head, render, manifest, plugins, utils } = require('./config')
const {
  baseUrl: base,
  rootDir,
  srcDir,
  isDev,
  target,
  modern,
  loading,
  css,
  telemetry,
  components,
  styleResources,
  googleFonts,
} = utils

// const log = []
// let dev

console.log(chalk.bgYellowBright.black(' DEV MODE '), isDev ? 'ON' : 'OFF')
console.log(chalk.bgMagentaBright.black(' DEBUG '), process.env.DEBUG)
console.log(chalk.bgBlueBright.black(' BASE URL '), base)

export default {
  rootDir,
  srcDir,
  target,
  modern,
  build,
  head,
  manifest,
  plugins,
  render,
  loading,
  css,
  telemetry,
  components,
  watch: ['@/../.env*', '@/../config'],
  env: { ...env.parsed },
  router: { base },

  generate: {
    fallback: false,
    exclude: ['/catalog/cart', '/catalog/compare', '/catalog/wishlist'],
    routes: isDev ? ['/', '/404'] : [...['/', '/404'], ...require(resolve(rootDir, `routes.json`))],
  },

  buildModules: [
    '@nuxtjs/eslint-module',
    // '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
    // '@nuxtjs/moment',
    '@nuxtjs/style-resources',
    // 'nuxt-purgecss',
    [
      'nuxt-compress',
      {
        gzip: {
          cache: true,
        },
      },
    ],
  ],

  modules: [
    '@nuxtjs/axios',
    // '@nuxtjs/apollo',
    // '@nuxtjs/pwa',
    // '@nuxt/content',
    // '@nuxtjs/recaptcha',
    // '@nuxtjs/auth',
    'nuxt-i18n',
    // 'nuxt-session',
    // '@nuxtjs/vendor',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    [
      '@naumstory/nuxtjs-yandex-metrika',
      {
        id: getenv('YANDEX_METRIKA_ID'),
        webvisor: true,
        clickmap: true,
        // useCDN:false,
        trackLinks: true,
        accurateTrackBounce: true,
      },
    ],
    'nuxt-webfontloader',

    // --------------------------
    // 'nuxt-memwatch',
    // --------------------------

    // 'nuxt-sweetalert2',
    // 'nuxt-webpackdashboard',
    // '~/modules/api',
  ],

  /* hooks: {
    listen(server, listener) {
      dev = listener.dev
    },
    render: {
      routeDone(url, res, context) {
        log.push(res)

        // development mode significantly increases used memory already
        // and it crashes around ~3k requests anyway
        if (log.length > (dev ? 100 : 18000)) {
          log = []
        }
      },
    },
  }, */

  // ---------------------------------------------------------------------------

  /* auth: {
    localStorage: false,
    cookie: {
      options: {
        expires: 7,
      },
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/auth/token', method: 'post', propertyName: 'access_token' },
          logout: false,
          user: { url: '/auth/user/me', method: 'get', propertyName: false },
        },
      },
    },
    plugins: ['~/plugins/axios.js', { src: '~/plugins/auth.js', mode: 'client' }],
  }, */

  axios: {
    debug: getenv.bool('AXIOS_DEBUG'),
    https: getenv.bool('AXIOS_HTTPS'),
    proxy: getenv.bool('AXIOS_PROXY'),
  },

  i18n: {
    strategy: 'no_prefix',
    defaultLocale: getenv('APP_LOCALE'),
    locales: [
      { name: 'Русский', code: 'ru', iso: 'ru-RU', file: 'ru.js' },
      // { name: 'English', code: 'en', iso: 'en-US', file: 'en.js' },
    ],
    langDir: 'locales/',
    lazy: true,
    seo: true,
    vueI18nLoader: true,
    detectBrowserLanguage: {
      alwaysRedirect: false,
      fallbackLocale: getenv('APP_LOCALE'),
      onlyOnRoot: true,
      useCookie: false,
    },
    vueI18n: {
      fallbackLocale: getenv('APP_LOCALE'),
      numberFormats: {
        ru: {
          currency: {
            style: 'currency',
            currency: 'RUB',
            currencyDisplay: 'symbol',
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
          },
        },
        en: {
          currency: {
            style: 'currency',
            currency: 'USD',
          },
        },
      },
    },
  },

  memwatch: {
    graph: true,
    gcMetrics: false,
    gcAfterEvery: 0,
    autoHeapDiff: true,
    useMovingAverage: 5,
    graphSetup(graphSetup) {
      graphSetup.metrics.malloc = {
        aggregator: 'avg',
        color: 'cyan',
      }
    },
    graphAddMetric(graph, stats) {
      graph.metric('my metrics', 'malloc').push(stats.malloced_memory)
    },
  },

  moment: {
    locales: ['ru'],
  },

  sitemap: {
    hostname: getenv('APP_HOST'),
    trailingSlash: true,
    gzip: true,
  },

  styleResources,

  robots: {
    Host: getenv('APP_HOST'),
  },

  vue: {
    config: {
      productionTip: false,
      ignoredElements: ['vue-ignore', 'noindex', 'code', 'pre'],
    },
  },

  vuetify: {
    /* frameworkOptions: {
      theme: {
        dark: false,
        themes: {
          dark: {
            // primary: colors.blue.darken2,
            // accent: colors.grey.darken3,
            // secondary: colors.amber.darken3,
            // info: colors.teal.lighten1,
            // warning: colors.amber.base,
            // error: colors.deepOrange.accent4,
            // success: colors.green.accent3,
          },
        },
      },
    },
    loader: {
      registerStylesSSR: true,
    },
    defaultAssets: false, */
    // customVariables: ['~/assets/styles/_variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {},
        light: {},
      },
      options: {
        // customProperties: true,
        customProperties: false,
        minifyTheme: css => (process.env.NODE_ENV === 'production' ? css.replace(/[\r\n|\r|\n]/g, '') : css),
      },
    },
    /* lang: {
      locales: { ru },
      current: 'ru',
    } */
    // treeShake: true,
  },

  webfontloader: {
    google: { families: googleFonts },
  },
}
