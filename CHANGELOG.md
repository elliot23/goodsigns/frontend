# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.9.5-beta.15](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.14...v0.9.5-beta.15) (2020-10-29)

### Features

- upd error ([c25da13](https://gitlab.com/elliot23/goodsigns/frontend/commit/c25da13c1218f6fdd3a96050fd8d276057e21a34))

### [0.9.5-beta.14](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.13...v0.9.5-beta.14) (2020-10-05)

### [0.9.5-beta.13](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.12...v0.9.5-beta.13) (2020-10-05)

### [0.9.5-beta.12](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.11...v0.9.5-beta.12) (2020-10-05)

### Features

- upd lint ([95f2f71](https://gitlab.com/elliot23/goodsigns/frontend/commit/95f2f71345a619b960ae68365ca999468b51c656))
- upgrade ([4c7917a](https://gitlab.com/elliot23/goodsigns/frontend/commit/4c7917aef5ab440ccbe443b653cabacccc3ec248))
- upgrade ([19a6db0](https://gitlab.com/elliot23/goodsigns/frontend/commit/19a6db0ad02fb72e368daf6aec366e28cb667df7))
- upgrade config ([ef91e7d](https://gitlab.com/elliot23/goodsigns/frontend/commit/ef91e7d7bb2b0bad4bd04bc9df19a6b411a2b3ba))
- upgrade mixins & utils ([0bf0838](https://gitlab.com/elliot23/goodsigns/frontend/commit/0bf083889d441d7cac23e9c370cd4068e56c107b))

### [0.9.5-beta.11](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.10...v0.9.5-beta.11) (2020-09-24)

### Features

- upgrade config ([42f4943](https://gitlab.com/elliot23/goodsigns/frontend/commit/42f494362dc9c76adfc370371741d3a927da8e9b))

### [0.9.5-beta.10](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.9...v0.9.5-beta.10) (2020-09-24)

### Features

- new release beta ([2254683](https://gitlab.com/elliot23/goodsigns/frontend/commit/2254683c8f0604a3cd0c281369f28e9c41748497))

### [0.9.5-beta.9](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.7...v0.9.5-beta.9) (2020-09-24)

### Bug Fixes

- testing ([2eed77b](https://gitlab.com/elliot23/goodsigns/frontend/commit/2eed77bb9e2a78e7c5f7c8b8c22f6cb1671fa973))

### [0.9.5-beta.8](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.7...v0.9.5-beta.8) (2020-09-24)

### Bug Fixes

- testing ([2eed77b](https://gitlab.com/elliot23/goodsigns/frontend/commit/2eed77bb9e2a78e7c5f7c8b8c22f6cb1671fa973))

### [0.9.5-beta.7](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.6...v0.9.5-beta.7) (2020-07-07)

### Features

- **pages:** upgraded catalog ([acbf5a6](https://gitlab.com/elliot23/goodsigns/frontend/commit/acbf5a6a007a4c88d6a2e3331a08cd44fae0970b))
- added Feedback component ([216e027](https://gitlab.com/elliot23/goodsigns/frontend/commit/216e0270ad5be939bee2a1663e30badf75b62a6b))
- **components:** added content components ([1f6bef4](https://gitlab.com/elliot23/goodsigns/frontend/commit/1f6bef4adebc5586ed2d472eb85ca436ff5773cc))
- **components:** updated header & footer ([5d787b8](https://gitlab.com/elliot23/goodsigns/frontend/commit/5d787b895ed3f4b189c73d4136b67d283d51e222))

### [0.9.5-beta.6](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.5...v0.9.5-beta.6) (2020-07-07)

### [0.9.5-beta.5](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.4...v0.9.5-beta.5) (2020-07-07)

### Features

- upgrade components, pages, layouts ([9f0ece4](https://gitlab.com/elliot23/goodsigns/frontend/commit/9f0ece455702cc7fa5146b8b438887fc4cf4f114))
- **script:** update create-env ([6266cbb](https://gitlab.com/elliot23/goodsigns/frontend/commit/6266cbbc7f9f66be50ca704d7490cb252b626bfc))
- **script:** update oasis/convert2db ([16cd781](https://gitlab.com/elliot23/goodsigns/frontend/commit/16cd78135803c0dcc0fe724325ac9578af7d8c06))
- add script/oasis ([cb7497b](https://gitlab.com/elliot23/goodsigns/frontend/commit/cb7497b52669ab652ed3aedcb309e83f71133485))

### [0.9.5-beta.4](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.3...v0.9.5-beta.4) (2020-06-19)

### Features

- add oasis2 ([4fcb286](https://gitlab.com/elliot23/goodsigns/frontend/commit/4fcb2869b897f3c0d2a08a705cffbac687a2d820))

### [0.9.5-beta.3](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.2...v0.9.5-beta.3) (2020-06-06)

### Features

- **layouts:** upd default ([86c434b](https://gitlab.com/elliot23/goodsigns/frontend/commit/86c434b69db3851ec7cf340dbf12894a59b2cfe3))
- **pages:** upd catalog & cart ([9dc4d3a](https://gitlab.com/elliot23/goodsigns/frontend/commit/9dc4d3a34b6a48ea768992e8686605d493cd221b))

### [0.9.5-beta.2](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.0...v0.9.5-beta.2) (2020-06-06)

### Features

- **api:** upgrade & added es5 support ([c971cce](https://gitlab.com/elliot23/goodsigns/frontend/commit/c971cce798746f7a049d6daf352e043859339f9d))
- **components:** new components ([6b160a1](https://gitlab.com/elliot23/goodsigns/frontend/commit/6b160a1bab43910b873c776a84ad40120431a3cf))
- **components:** upd header & footer ([fc0733d](https://gitlab.com/elliot23/goodsigns/frontend/commit/fc0733d2a0e4e575b6dfb6524b05eaeb55aab8fc))
- **pages/\_slug:** upgraded ([8d94ec6](https://gitlab.com/elliot23/goodsigns/frontend/commit/8d94ec64338beb06b8012a4405975240c4367586))
- upd utils ([f8a18b8](https://gitlab.com/elliot23/goodsigns/frontend/commit/f8a18b8fac590846e71f61204a9b3b32b2302984))

### Bug Fixes

- **nuxt.config:** saving a log file ([dad9c55](https://gitlab.com/elliot23/goodsigns/frontend/commit/dad9c5516d6ef7af23f6d4ea8f08f75eed0ca892))

### [0.9.5-beta.1](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.5-beta.0...v0.9.5-beta.1) (2020-05-27)

### Features

- upgrade ([822d371](https://gitlab.com/elliot23/goodsigns/frontend/commit/822d371d4046974e5732145fdf656db236175484))
- upgrade catalog ([c5630fd](https://gitlab.com/elliot23/goodsigns/frontend/commit/c5630fd232b6747f36e882c26cdc85a67ac1ece4))
- upgrade page ([c557fc7](https://gitlab.com/elliot23/goodsigns/frontend/commit/c557fc7b603c52c7589c6952155f091c20927f34))
- **pages/slug:** upgrade ([8fb5c42](https://gitlab.com/elliot23/goodsigns/frontend/commit/8fb5c423a702bb68b64d619a9a922b4a134e7a86))
- **utils:** logs, safeStringify ([d4934ac](https://gitlab.com/elliot23/goodsigns/frontend/commit/d4934ac461ef0bb88e6c2eff5abd6d3f548a300d))

### [0.9.5-beta.0](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.4...v0.9.5-beta.0) (2020-05-14)

### Features

- **pages:** add contact-us ([a4a1d73](https://gitlab.com/elliot23/goodsigns/frontend/commit/a4a1d73aadcb2c3a8253886a48cc9c5a9ae9e43d))

### Bug Fixes

- **nuxt.config:** env.MENU_KEYS ([50fa73e](https://gitlab.com/elliot23/goodsigns/frontend/commit/50fa73ebe42ae933f7289d4a2adffbf56b13bf21))

### [0.9.4](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.3...v0.9.4) (2020-04-30)

### Features

- add user menu ([b5e02e7](https://gitlab.com/elliot23/goodsigns/frontend/commit/b5e02e7c01acb18d0d64ba91d4c7a86f911864b0))
- upgrade catalog/category page ([3491c57](https://gitlab.com/elliot23/goodsigns/frontend/commit/3491c57b2e0d9ecc959b8eb8da00f8d0ce37ab99))
- upgrade categories tree ([261b286](https://gitlab.com/elliot23/goodsigns/frontend/commit/261b286e1c9b8b5003db61253072891a027280b7))

### [0.9.3](https://gitlab.com/elliot23/goodsigns/frontend/compare/v0.9.2...v0.9.3) (2020-04-29)

### 0.9.2 (2020-03-30)

### Features

- **api:** expressjs-cockpit rest api ([e01044e](https://gitlab.com/elliot23/goodsigns/frontend/commit/e01044e32e06d1b1448a6f7575f8dfb2be44f6ce))
- add components/layout ([bc6da52](https://gitlab.com/elliot23/goodsigns/frontend/commit/bc6da5226d6ef97c0b5c68caf07373c741518163))
- add pages/\_slug ([4450c9b](https://gitlab.com/elliot23/goodsigns/frontend/commit/4450c9b64abc28bedc3968a0bbfbff360e249485))
- add plugins & utils ([f9b8ea2](https://gitlab.com/elliot23/goodsigns/frontend/commit/f9b8ea258322ade36f53af96479629460330e0ef))
- add sandbox ([fbcc356](https://gitlab.com/elliot23/goodsigns/frontend/commit/fbcc3568fefeb5e6231d63a23462178c6945b250))
- cockpit api ([ab718be](https://gitlab.com/elliot23/goodsigns/frontend/commit/ab718bec180cccbb81102b256a761af8b4e3fb22))
- cockpit api ([e382355](https://gitlab.com/elliot23/goodsigns/frontend/commit/e382355496e0d552ac924d69fcc49e47a8fd614a))
- test1 ([f774ff1](https://gitlab.com/elliot23/goodsigns/frontend/commit/f774ff19e12d6d561c1e9cf6d3ce3908b1263121))
- test2 ([b0356c4](https://gitlab.com/elliot23/goodsigns/frontend/commit/b0356c4ec2cf53799e57a75d3a5ce6adfb70941d))
- test3 ([3c15cb2](https://gitlab.com/elliot23/goodsigns/frontend/commit/3c15cb2db6d9392829999a00145c650340297d67))
- upd ([7cf3c73](https://gitlab.com/elliot23/goodsigns/frontend/commit/7cf3c73710e3392f0464ab4b99b43ee3bdf8ac3b))

### Bug Fixes

- eslint rules ([f378237](https://gitlab.com/elliot23/goodsigns/frontend/commit/f378237d0f692dd9842a3853eac27ff7c03eeb14))
- eslint, package.json ([a4b4807](https://gitlab.com/elliot23/goodsigns/frontend/commit/a4b48072a10d310a01b9012218b092eee733d657))
- **eslint:** remove ecmaFeatures ([7033b81](https://gitlab.com/elliot23/goodsigns/frontend/commit/7033b81e19ddf18601ec3df0c3907e28a3727ea6))
- **lint:** arrowParens ([0e0c0e3](https://gitlab.com/elliot23/goodsigns/frontend/commit/0e0c0e3a1ec56b2271408c282e255629d2ee42db))
- env ([89823bc](https://gitlab.com/elliot23/goodsigns/frontend/commit/89823bc020f8ca387baeb36a80ffa8f93711a7c3))
- git-cz ([6e89ee1](https://gitlab.com/elliot23/goodsigns/frontend/commit/6e89ee1dad119fb88fc08b4a4d35f7c83d8b0953))
- husky ([7c81e32](https://gitlab.com/elliot23/goodsigns/frontend/commit/7c81e328434c1695eb25e8d989ec09f25532deba))
- husky ([f5644a2](https://gitlab.com/elliot23/goodsigns/frontend/commit/f5644a27768f3934d908dac977bfdb1cd245a74f))
- layout components ([1a45df8](https://gitlab.com/elliot23/goodsigns/frontend/commit/1a45df889e73015b8798455b24ea81e9ceb9ea38))
- lint ([1b28487](https://gitlab.com/elliot23/goodsigns/frontend/commit/1b28487a952f83c0fe7210b3bf889de0e86ea882))
- lint-staged ([a182268](https://gitlab.com/elliot23/goodsigns/frontend/commit/a182268ba1943a8c59c9b58000bb75bc900a244e))
- mv plugins/api ([ec959ac](https://gitlab.com/elliot23/goodsigns/frontend/commit/ec959acfbe0cd1c17c004b118783ee5b473e6c6c))
- package.json ([321288c](https://gitlab.com/elliot23/goodsigns/frontend/commit/321288c413bc75107047e2c2114986436d753d85))
- pkg ([4723806](https://gitlab.com/elliot23/goodsigns/frontend/commit/47238066d92e1e405669df469fec9ac522aeef22))
- pkg ([ffa2c75](https://gitlab.com/elliot23/goodsigns/frontend/commit/ffa2c7554f55295f14c0a79fd48ace049644b23a))
- rm tests ([50505f4](https://gitlab.com/elliot23/goodsigns/frontend/commit/50505f4a12484b9509b4418c1c61b1939f00e4ce))
- upd ([6d3079c](https://gitlab.com/elliot23/goodsigns/frontend/commit/6d3079c8c986bfe4fc30fe21de5b660285887f06))
- upd ([3747fad](https://gitlab.com/elliot23/goodsigns/frontend/commit/3747fad8ed4131cc0ecfa89d5d88f9ac73306783))
- upd ([27faa17](https://gitlab.com/elliot23/goodsigns/frontend/commit/27faa1797a53569b7d5611043006975f453fa320))
