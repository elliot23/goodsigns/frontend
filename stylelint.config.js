module.exports = {
  ignoreFiles: ['node_modules/**', '.plop/**', '.nuxt/**', 'build/**'],
  extends: ['stylelint-config-standard', 'stylelint-config-idiomatic-order'],
  rules: {
    // 'value-no-vendor-prefix': true
  },
}
