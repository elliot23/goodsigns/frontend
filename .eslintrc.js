const OFF = 0
const WARN = 1
const ERROR = 2

// Gist fork: https://gist.github.com/mantissa23/53b1de222a798e5ef542f87b5ce0f8ad

module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaVersion: 2020,
  },
  extends: ['@nuxtjs', 'prettier', 'prettier/vue', 'plugin:prettier/recommended', 'plugin:nuxt/recommended'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': [ERROR],

    // All JSDoc comments must be valid
    'valid-jsdoc': [
      // ERROR,
      OFF,
      {
        requireReturn: false,
        requireReturnDescription: false,
        requireParamDescription: false,
        prefer: {
          return: 'returns',
        },
      },
    ],

    // Allowed a getter without setter, but all setters require getters
    'accessor-pairs': [
      ERROR,
      {
        getWithoutSet: false,
        setWithoutGet: true,
      },
    ],

    /* 'comma-dangle': [
      ERROR,
      {
        arrays: 'always-multiline',
        exports: 'always-multiline',
        functions: 'never',
        imports: 'always-multiline',
        objects: 'always-multiline',
      },
    ], */

    'class-methods-use-this': OFF,
    'implicit-arrow-linebreak': OFF,
    'import/named': OFF,
    'import/no-extraneous-dependencies': OFF,
    'import/prefer-default-export': OFF,
    'import/namespace': OFF,
    'nuxt/no-cjs-in-config': OFF,

    // Strict Mode - for ES6, never use strict.
    strict: [ERROR, 'never'],

    // Variables
    'no-unused-vars': WARN,
    'no-underscore-dangle': OFF,
    'no-console': OFF,
    'no-debugger': OFF,
    'no-shadow': OFF,
    'no-param-reassign': OFF,
    'no-prototype-builtins': OFF,
    'no-unreachable': OFF,

    // Use path.concat instead
    'no-path-concat': ERROR,
    'no-process-exit': ERROR,
    'no-restricted-modules': OFF,
    'no-sync': OFF, // WARN,

    // Vue
    'vue/component-name-in-template-casing': [
      'error',
      'kebab-case',
      {
        registeredComponentsOnly: false,
        ignores: [],
      },
    ],
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'any',
          normal: 'never',
          component: 'never',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'vue/no-v-html': OFF,
    'vue/valid-v-model': OFF,
  },

  /* settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', './src'],
          ['~', './src'],
        ],
        extensions: ['.js', '.vue'],
      },
    },
  }, */

  overrides: [
    {
      files: ['test/**/*.js'],

      rules: {
        'global-require': OFF,
      },
    },
  ],
}
