export const state = () => ({
  categories: {
    flat: [],
    tree: [],
  },
  cart: [],
  wishlist: [],
  icons: {
    2892: 'mdi-headset',
    2931: 'mdi-bag-personal-outline',
    2953: 'mdi-fridge-outline',
    3021: 'mdi-notebook-outline',
    3058: 'mdi-pencil-outline',
    3070: 'mdi-tshirt-v-outline',
    3089: 'mdi-umbrella-outline',
    3095: 'mdi-weather-sunny',
    3140: 'mdi-image-filter-hdr',
    3167: 'mdi-basketball',
    3180: 'mdi-home-variant-outline',
    3208: 'mdi-face-outline',
    3225: 'mdi-face-woman-outline',
    3238: 'mdi-shield-account-outline',
    3257: 'mdi-car',
    3275: 'mdi-briefcase-account-outline',
    3517: 'mdi-package-variant-closed',
    3543: 'mdi-candycane',
    4047: 'mdi-rhombus-split',
    4197: 'mdi-baby-carriage',
  },
})

export const mutations = {
  SET_CATEGORIES(state, payload) {
    state.categories = payload
  },

  SET_CART(state, payload) {
    state.cart = payload
  },

  ADD_TO_CART(state, payload) {
    const { id, qty = 1 } = payload
    const { cart } = state
    const index = cart.findIndex(item => item.id === id)

    if (index >= 0) {
      cart[index].quantity += qty
    } else {
      cart.push({ id, quantity: qty, deleted: false })
    }

    state.cart = cart
  },

  SET_ITEM_DELETED(state, payload) {
    const { id = null, deleted = true } = payload
    const index = state.cart.findIndex(item => item.id === id)

    if (index >= 0) {
      state.cart[index].deleted = deleted
    }
  },

  ADD_TO_WISHLIST(state, payload) {
    state.wishlist.push(payload)
  },
}

export const actions = {
  async init({ commit }) {
    const categories = await this.$api('v1').request('oasis/categories')
    commit('SET_CATEGORIES', categories)
  },
}
