export const state = () => ({
  drawer: false,
  elevation: 20,
  width: 320,
})

export const mutations = {
  SET_DRAWER(state, payload = false) {
    state.drawer = payload
  },

  SET_ELEVATION(state, payload = 10) {
    state.elevation = payload
  },

  SET_WIDTH(state, payload = 300) {
    state.width = payload
  },
}
