export const state = () => ({
  loading: false,
  themeDark: false,
  cookieNotification: false,

  snackbar: {
    visible: false,
    color: 'info',
    text: '',
    timeout: 3000,
    absolute: false,
    centered: false,
    top: false,
    bottom: true,
    left: true,
    right: false,
    width: 'auto',
    minWidth: 100,
    maxWidth: 1600,
  },

  winScrollPos: 0,
  winScrollDirty: false,

  visitedPages: [],

  layout: {
    locale: [],
  },
  pages: [],
  assets: {},

  mainMenu: {
    items: [],
    locked: false,
    selected: null,
    intersecting: null,
  },

  feedbackForm: {
    drawer: false,
    elevation: 20,
    width: 320,
  },

  orderForm: {
    drawer: false,
    elevation: 20,
    width: 320,
  },
})

export const mutations = {
  SET_LOADING(state, loading = true) {
    state.loading = loading
  },

  SET_THEME_DARK(state, darked = true) {
    state.themeDark = Boolean(darked)
  },

  SET_COOKIE_NOTIFICATION(state, payload = true) {
    state.cookieNotification = Boolean(payload)
  },

  SET_SNACKBAR(state, payload) {
    if (typeof payload.visible === 'undefined') payload.visible = true

    state.snackbar = {
      ...state.snackbar,
      ...payload,
    }
  },

  SET_LAYOUT(state, payload) {
    state.layout = payload
  },

  SET_PAGES(state, payload) {
    state.pages = payload
  },

  SET_ASSETS(state, payload) {
    state.assets = payload
  },

  SET_WIN_SCROLL_POS(state, payload) {
    state.winScrollDirty = true
    state.winScrollPos = parseInt(payload) || 0
  },

  SET_VISITED_PAGES(state, payload) {
    state.visitedPages = payload
  },

  ADD_VISITED_PAGES(state, payload) {
    state.visitedPages.push(payload)
  },

  SET_FEEDBACK_FORM(state, payload) {
    state.feedbackForm = {
      ...state.feedbackForm,
      ...payload,
    }
  },

  SET_ORDER_FORM(state, payload) {
    state.orderForm = {
      ...state.orderForm,
      ...payload,
    }
  },

  SET_MAIN_MENU(state, payload) {
    state.mainMenu = {
      ...state.mainMenu,
      ...payload,
    }
  },
}

export const actions = {
  async nuxtServerInit({ dispatch }, { req, error, route }) {
    await dispatch('initApi')
    await dispatch('catalog/init')
  },

  async initApi({ commit, dispatch }) {
    // dispatch('startLoading')
    await this.$api('v1').init({ pages: { fieldset: 'full' } })
    // dispatch('finishLoading')
  },

  startLoading({ commit }) {
    if (process.browser) {
      window.$nuxt.$root.$loading.start()
    }
    commit('SET_LOADING', true)
  },

  finishLoading({ commit }) {
    if (process.browser) {
      window.$nuxt.$root.$loading.finish()
    }
    commit('SET_LOADING', false)
  },
}

export const getters = {
  layoutLocale: state => state.layout.locale.reduce((acc, cur) => ({ ...acc, [cur.value.key]: cur.value.val }), {}),

  layoutMenu: state => name => {
    const menu = state.layout[name] || null

    if (!menu) return []
    return menu
      .filter(item => item.value.published)
      .map(item => {
        const { value } = item
        const { page: valPage } = value
        const page = 'page' in value ? state.pages.find(page => page._id === value.page._id) : {}
        const title = value.display || page.display || valPage.display || null
        const link = ('link' in value ? value.link : `/${page.slug}`) || '#'

        return { title, link }
      })
      .filter(item => item.title !== null)
  },
}
