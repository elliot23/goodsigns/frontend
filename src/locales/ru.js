export default {
  heading: {
    catalog: 'Каталог товаров',
    cart: 'Корзина покупок',
    'order-registration': 'Оформление заказа',
    'order-confirmation': 'Подтверждение заказа',
    'filter-price': 'Цена',
    materials: 'Материалы',
    colors: 'Цвета',
    sizes: 'Размеры',
    attributes: 'Характеристики',
    'personal-data': 'Ваши данные',
  },
  button: {
    'all-categories': 'Все категории',
    'add-to-cart': 'Добавить к заказу',
    'add-to-wishlist': 'Добавить в желания',
    'order-registration': 'Оформить заказ',
    continue: 'Продолжить',
    'go-to-back': 'Вернуться',
    'go-to-home': 'Вернуться на главную',
    'get-additive': 'Еще',
    'undo-delete': 'Восстановить',
  },
  text: {
    wishlist: 'Избранные товары',
    compare: 'Товары для сравнения',
    cart: 'Корзина покупок',
    search: 'Поиск по сайту',
    categories: 'нет категорий | категория | категории | категорий',
    'category-emptry': 'В этой категории пока нет товаров',
    'product-article': 'Артикул',
    'product-available': 'Есть в наличии',
    'product-not-available': 'Товара нет в наличии',
    'product-to-cart-qty': 'Для заказа выбрано',
    'product-to-cart-sum': 'на сумму',
    'product-total': 'нет изделий | изделие | изделия | изделий',
    'product-qty-invalid': 'Вы забыли указать кол-во нужного размера',
    from: 'От',
    to: 'До',
    'cart-empty': 'В вашей корзине пока пусто',
    'cart-item-added': 'Товар добавлен в корзину',
    'cart-item-deleted': 'Товар удален',
    'total-price': 'Итого',
    'field-name': 'Ваше имя',
    'field-email': 'E-mail',
    'field-phone': 'Телефон',
    'field-comment': 'Комментарий',
    size: 'Размер',
    qty: 'Кол-во',
    stock: 'На складе',
  },
  validation: {
    requied: 'Вы забыли заполнить это поле',
    email: 'Некорректный e-mail адрес',
  },
  error: {
    default: 'Упс! Что-то сломалось 🥵',
    'not-found': 'Такой страницы у нас нет 😢',
  },
}
