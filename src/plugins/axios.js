/* import { cacheAdapterEnhancer } from 'axios-extensions'
import LRUCache from 'lru-cache'

const ONE_HOUR = 1000 * 60 * 60

export default function({ app, env, redirect }) {
  // app.$axios.onError(error => console.log(error));
  // app.$axios.setHeader('Cockpit-Token', env.API_TOKEN);
  // app.$axios.setToken(env.apiToken, 'Bearer');

  const defaultCache = new LRUCache({ maxAge: ONE_HOUR })
  const { defaults } = app.$axios

  defaults.adapter = cacheAdapterEnhancer(defaults.adapter, false, 'useCache', defaultCache)
}
 */

export default ({ $axios, store }) => {
  /* $axios.onResponse(response => {
    console.log(`[${response.status}] ${response.request.path}`)
  }) */

  $axios.onError(err => {
    console.log(`[${err.response && err.response.status}] ${err.response && err.response.request.path}`)
    console.log(err.response && err.response.data)
  })
}
