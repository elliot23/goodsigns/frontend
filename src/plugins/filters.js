import Vue from 'vue'
import { host, pluralize, formattedCost, shortenNum } from '~/utils'

const filters = { host, pluralize, formattedCost, shortenNum }

export default filters

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
