import CockpitContentAPI from '~/utils/cockpit/content-api'

/* eslint-disable */
export default async (ctx, inject) => {
  const { env } = ctx
  const { COCKPIT_URL, COCKPIT_TOKEN, API_VERSION, API_PREFIX } = env

  const client = new CockpitContentAPI({
    url: COCKPIT_URL,
    token: COCKPIT_TOKEN,
    prefix: API_PREFIX,
    version: API_VERSION,
    httpClient: ctx.$axios
  })

  ctx.$cockpit = client
  inject('cockpit', client)
}
