import VuexPersistence from 'vuex-persist'
import Cookies from 'js-cookie'

export default ({ store }) => {
  new VuexPersistence({
    // storage: window.localStorage,
    // paths: ['auth'],
    storage: {
      getItem: key => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key),
    },
    reducer: state => ({
      themeDark: state.themeDark,
      catalog: {
        cart: state.catalog.cart,
        wishlist: state.catalog.wishlist,
      },
      // filter: mutation => mutation.type === 'ADD_TO_CART',
    }),
  }).plugin(store)
}
