<template>
  <div :id="settings.id || null" :class="settings.class" :style="settings.style">
    <v-img v-for="(src, i) in images" :key="`com-gallery__img-${i}`" :src="src"></v-img>
  </div>
</template>

<script>
export default {
  props: {
    layout: {
      type: Object,
      required: true,
    },
  },
  data: () => ({
    images: [],
  }),

  computed: {
    settings() {
      return this.layout.settings
    },
  },

  created() {
    if (process.client) {
      this.loadImages()
    }
  },

  methods: {
    async loadImages() {
      const data = await Promise.all(
        this.settings.gallery.map(img =>
          this.$api('v1').request('cockpit/image', { method: 'POST', data: { src: img.path } })
        )
      )
      this.images = data
    },
  },
}
</script>
