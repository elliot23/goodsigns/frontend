import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

export default function ({ env }) {
  return {
    link: new HttpLink({ uri: `${env.APOLLO_ENDPOINT}?token=${env.APOLLO_TOKEN}` }),
    cache: new InMemoryCache(),
    defaultHttpLink: false,
  }
}
