/**
 * Случайное число в промежутке min-max
 * @param {Number} min
 * @param {Number} max
 * @returns {Number}
 */
export default function randomRange(min = 0, max = 100) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}
