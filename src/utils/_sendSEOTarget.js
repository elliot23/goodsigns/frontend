/**
 * Отправка целей
 * Для проверки работы цели YM добавить в УРЛ параметр ?_ym_debug=1
 * @param {Object} yandexMetrika
 * @param {Number} yandexMetrika.counterID ID счетчика
 * @param {String} yandexMetrika.reachGoal Имя цели
 * @param {Object} googleAnalytics
 * @param {String} googleAnalytics.eventCategory Объект, с которым взаимодействовал пользователь (например, 'Video')
 * @param {String} googleAnalytics.eventAction Тип взаимодействия (например 'play')
 */
export default function sendSEOTarget(yandexMetrika = null, googleAnalytics = null) {
  if (yandexMetrika) {
    try {
      window[`yaCounter${yandexMetrika.counterID}`].reachGoal(yandexMetrika.reachGoal)
    } catch (error) {
      console.error(
        error,
        `\n[Yandex Metrika] counterID: ${yandexMetrika.counterID}, ` + `reachGoal: ${yandexMetrika.reachGoal}`
      )
    }
  }
  if (googleAnalytics) {
    try {
      if (window.ga instanceof Function) {
        window.ga('send', 'event', googleAnalytics.eventCategory, googleAnalytics.eventAction)
      } else {
        console.info('window.ga must be a function')
      }
    } catch (error) {
      console.error(
        error,
        '\n[Google Analytics] eventCategory: ' +
          `${googleAnalytics.eventCategory}, eventAction: ` +
          `${googleAnalytics.eventAction}`
      )
    }
  }
}
