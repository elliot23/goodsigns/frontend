/**
 * Конверт цвета из rgb в hex
 * @param {Array} rgb
 * @returns {String}
 */
export default function rgbToHex(rgb) {
  const [r, g, b] = rgb
  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
}
