/**
 * Перемешивание элементов массива
 * @param {Array}
 * @returns {Array}
 */
export default function shuffleArray(array) {
  return array
    .map(a => [Math.random(), a])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1])
}
