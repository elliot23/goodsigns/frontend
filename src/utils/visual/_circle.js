export default class Circle {
  constructor(ctx, size, pos, color) {
    this.ctx = ctx
    this.size = size
    this.pos = pos
    this.r = 1.5 * Math.random() + 1
    this.c = color
    this.v = [(Math.random() - 0.5) * 0.3, (Math.random() - 0.5) * 0.3]
  }

  getBound(i) {
    return this.size[i ? 'height' : 'width']
  }

  frame() {
    for (let i = 0; i < 2; i++) {
      if (this.pos[i] > this.getBound(i) - 10) {
        this.v[i] *= -1
      } else if (this.pos[i] < 10) {
        this.v[i] *= -1
      }
      this.pos[i] += this.v[i] * 10
    }

    this.draw()
  }

  draw() {
    this.ctx.fillStyle = this.c
    this.ctx.beginPath()
    this.ctx.arc(this.pos[0], this.pos[1], this.r, 0, 2 * Math.PI, false)
    this.ctx.fill()
  }
}
