/**
 * Создает массив чисел (положительных и / или отрицательных),
 * прогрессирующих от начала до конца, но не включая его.
 * @param {*} start
 * @param {*} end
 * @param {*} step
 */
export default function* range(start = 0, end = undefined, step = 1) {
  if (arguments.length === 1) {
    // eslint-disable-next-line no-unused-expressions, no-sequences
    ;(end = start), (start = 0)
  }

  ;[...arguments].forEach(arg => {
    if (typeof arg !== 'number') {
      throw new TypeError('Invalid argument')
    }
  })
  if (arguments.length === 0) {
    throw new TypeError('range requires at least 1 argument, got 0')
  }

  if (start >= end) return
  yield start
  yield* range(parseInt(start + step), end, step)
}
