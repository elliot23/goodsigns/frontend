/**
 * Склонение существительного после числительного
 * @param {Number} value
 * @param {Array} forms
 * @returns {String}
 * @example {{ count | pluralize(['магазин', 'магазинов']) }}
 * @example {{ count | pluralize(['магазин', 'магазина', 'магазинов']) }}
 */
export default function pluralize(value, forms) {
  let n = Math.abs(+value)

  n %= 100
  if (n >= 5 && n <= 20) return forms[2] || forms[1]
  n %= 10
  if (n === 1) return forms[0]
  if (n >= 2 && n <= 4) return forms[1]

  return forms[2] || forms[1]
}
