/**
 * Форматирование триад
 * @param {Number|String} value
 * @returns {String}
 */
export default function formattedCost(value = 0) {
  return value
    .toString()
    .replace(/\..+$/, '')
    .replace(/\D/g, '')
    .replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
}
