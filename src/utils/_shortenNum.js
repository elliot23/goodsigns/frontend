/**
 * Сокращение числа после тысячи
 *
 * @param {number} num
 * @param {number} [digits=0]
 * @returns {string|number}
 */
export default function shortenNum(num, digits) {
  const units = ['k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y']
  for (let i = units.length - 1; i >= 0; i--) {
    const decimal = 1000 ** (i + 1)

    if (num <= -decimal || num >= decimal) {
      return +(num / decimal).toFixed(digits) + units[i]
    }
  }

  return num
}
