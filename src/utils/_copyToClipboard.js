/**
 * Копирование строки в буффер обмена
 * @param {String} string
 */
export default function copyToClipboard(string) {
  const el = document.createElement('textarea')

  el.value = string
  el.setAttribute('readonly', '')
  document.body.appendChild(el)
  el.select()
  document.execCommand('copy')
  document.body.removeChild(el)
}
