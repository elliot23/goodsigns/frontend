/**
 * Возвращает имя хоста из строки (url)
 * @param {String} url
 * @returns {String}
 */
export default function host(url) {
  const host = url
    .replace(/^https?:\/\//, '')
    .replace(/\/.*$/, '')
    .replace('?id=', '/')
  const parts = host.split('.').slice(-3)
  if (parts[0] === 'www') parts.shift()
  return parts.join('.')
}
