const ANIM_DEFAULT = 'bounce'

export default (element, animation = null, duration = null, prefix = 'animate__') =>
  new Promise((resolve, reject) => {
    const node = typeof element === 'string' ? document.querySelector(element) : element

    if (node !== null) {
      const { anim } = node.dataset
      const animationName = `${prefix}${!anim && !animation ? ANIM_DEFAULT : anim && !animation ? anim : animation}`

      node.classList.add(`${prefix}animated`, animationName)

      if (duration) {
        node.style.setProperty('--animate-duration', `${duration}ms`)
      }

      const handleAnimationEnd = () => {
        node.classList.remove(`${prefix}animated`, animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        resolve('Animation ended')
      }

      node.addEventListener('animationend', handleAnimationEnd)
    } else {
      console.log(`The node does not exist (${element})`)
    }
  })
