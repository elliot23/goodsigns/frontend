/**
 * Возвращает дерево из массива по ключу ooptions.kpid
 * @param {Array} arr
 * @param {Object} options
 * @returns {Array}
 */
export default function arrTree(arr, options = { kid: 'id', kpid: 'parent_id' }) {
  const { kid, kpid } = options
  const tree = []
  const mappedArr = {}
  let arrElem
  let mappedElem

  for (let i = 0, len = arr.length; i < len; i++) {
    arrElem = arr[i]
    mappedArr[arrElem[kid]] = arrElem
    mappedArr[arrElem[kid]].children = []
  }

  for (const id in mappedArr) {
    if (mappedArr.hasOwnProperty(id)) {
      mappedElem = mappedArr[id]
      if (mappedElem[kpid]) {
        mappedArr[mappedElem[kpid]].children.push(mappedElem)
      } else {
        tree.push(mappedElem)
      }
    }
  }

  return tree
}
