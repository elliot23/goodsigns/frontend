/**
 * Конверт цвета из hex в rgb
 * @param {String} hex
 * @returns {String}
 */
export default function hexToRGB(hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  if (result) {
    const rgb = [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)]
    return rgb.join(',')
  }
  return null
}
