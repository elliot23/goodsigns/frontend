import path from 'path'
// import fetch from 'node-fetch'
import nodeRes from 'node-res'
import consola from 'consola'

const logger = consola.withScope('api')

export default function (moduleOptions) {
  const { nuxt } = this

  const options = {
    ...nuxt.options.api,
    ...moduleOptions,
    ...(nuxt.options.runtimeConfig && nuxt.options.runtimeConfig.api),
  }

  this.addPlugin({
    src: path.resolve(__dirname, 'plugin.js'),
    fileName: 'api.js',
    options,
  })

  this.nuxt.hook('modules:done', mod => {
    console.log(mod)
  })

  const releases = ['r1', 'r2']

  this.addServerMiddleware({
    path: '/_routes',
    handler(req, res) {
      nodeRes.send(req, res, releases)
    },
  })

  logger.info('API MOD')
  logger.info('options', options)
}
