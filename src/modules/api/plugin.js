export default (ctx, inject) => {
  console.log(ctx.$axios)

  // ctx.$api = api
  // inject('api', api)
}

/* import { setupCache } from 'axios-cache-adapter'

const supportedVersions = ['v1', 'canary']
const supportedRoutes = ['cockpit']

const CACHE_MAX_AGE = 15 * 60 * 1000

export default (ctx, inject) => {
  const { store, $axios } = ctx

  const cache = setupCache({
    maxAge: CACHE_MAX_AGE,
  })

  const axiosInstance = $axios.create({
    adapter: cache.adapter,
  })

  const api = version => {
    if (!supportedVersions.includes(version)) {
      throw new Error('[API]: Config Invalid: api does not support the supplied version')
    }

    if (!supportedVersions.includes(version)) {
      throw new Error('[API]: Config Invalid: api does not support the supplied version')
    }

    const request = async (route, options = {}) => {
      if (!supportedRoutes.includes(route)) {
        // todo...
      }

      const url = `/${version}/${route}`
      const { method = 'GET', headers = {}, data = {}, params = {} } = options

      const response = await axiosInstance({ url, method, headers, data, params })
      return response.data
    }

    const fetchCockpitPages = async (payload = {}) => {
      const { fieldset = 'basic' } = payload
      const options = {
        method: 'POST',
        data: {
          fields: {
            title: 1,
            slug: 1,
            published: 1,
            image: 1,
            description: 1,
          },
        },
      }
      if (fieldset === 'full') delete options.data.fields

      const pages = await request('cockpit/pages', options)
      const { entries = [] } = pages

      store.commit('SET_PAGES', entries)
    }

    const fetchCockpitLayout = async (payload = {}) => {
      const headers = {
        // 'Cache-Control': 'public, max-age=31536000'
      }
      const layout = await request('cockpit/layout', { headers })
      store.commit('SET_LAYOUT', layout)
    }

    const fetchCockpitAssets = async (payload = {}) => {
      const headers = {
        // 'Cache-Control': 'public, max-age=31536000'
      }
      const assets = await request('cockpit/assets', { headers })
      store.commit('SET_ASSETS', assets.assets)
    }

    return {
      async init(options = {}) {
        const { pages = {}, layout = {}, assets = {} } = options

        await fetchCockpitPages(pages)
        await fetchCockpitLayout(layout)
        await fetchCockpitAssets(assets)
      },

      request,
    }
  }

  ctx.$api = api
  inject('api', api)
}
*/
