# Конфигурация

- :lizard:

The default options are:

```js
{
    // api folder tree directory -- required
    directory: __dirname + '/api',

    // api prefix path
    prefix: '/api',
}
```
