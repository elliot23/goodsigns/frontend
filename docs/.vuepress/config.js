module.exports = {
  base: '/docs/',
  locales: {
    '/': {
      lang: 'ru-ru',
      title: '@goodsigns.ru/frontend',
      description: '...descr...',
    },
  },
  themeConfig: {
    // repo: 'repo',
    /* plugins: [
      [
        '@vuepress/search',
        {
          searchMaxSuggestions: 10,
        },
      ],
    ], */
    search: true,
    searchMaxSuggestions: 10,

    locales: {
      '/': {
        /* nav: [
          {
            text: 'Languages',
            items: [{ text: 'Russian', link: '/' }],
          },
        ], */
        sidebar: [
          ['/', 'Введение'],
          ['/installation', 'Установка'],
          ['/configuration', 'Конфигурация'],
        ],
      },
    },
  },
}
