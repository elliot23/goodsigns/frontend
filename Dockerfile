FROM node:12.19.0-alpine

# create destination directory
RUN mkdir -p /usr/src/nuxtjs-app
WORKDIR /usr/src/nuxtjs-app

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

# copy the app, note .dockerignore
COPY . /usr/src/nuxtjs-app/
RUN yarn install
RUN yarn build

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "yarn", "start" ]