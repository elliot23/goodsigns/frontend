# TODO

## Major

- ProductsHome: грузить только которые в наличии
- добавить в head dns-prefetch
- Разложить компоненты из layout по папкам
- проверять версию sass-loader (должная быть не выше 7)
- env-generator: добавить сохранение .local файлов (в env.json ключ development.local...?)
- eslint (убрать vuetify)
- logger? consola? plugin?
- dotenv-flow - https://github.com/kerimdzhanov/dotenv-flow#readme
- Предупреждение про куки
- Определение системных требования (semver)
- Вынести конфиги husky и т.д. из package.json
- product.full_categories
- Добавить блок с предупреждением про куки
- Добавить изотоп в каталог
- :open_mouth: pages/\_slug, грузить полный список страниц без контента и лишних полей, в fetch подгружать остальные поля
- Убрать watch с папки server если запущен dev:api-only или dev:api-rnd
- Перенести локали (src/locale) в cockpit
- Вынести state.pages, state.layout в отдельный модуль store/cockpit (?)
- синхронизация корзины по email (redis?)
- маска для телефона
- у товару добавить цвета, упакову и нанесение
- Добавить Nuxt Stack - https://nuxtstack.org/
- Базовый компонент для components/content
- перенести скрипты из config/head и глоб. стили в env
- og метатеги - https://ruogp.me/
- head.link: dns-prefetch и preconnect на API
- render: http2 (from hackernews)
- RECAPTCHA

---

## Minor

- ...

---

## Cleaning

- ...

---

## Modules

- universal-storage-module
- @nuxtjs/amp
- nuxt-webpackdashboard
- nuxt-memwatch
- nuxt-sweetalert2

---

## Helpful

### Optimization

- https://habr.com/ru/post/425215/
