<p align="left">
<img align="center" style="width:123px" src="https://ma23.ru/_LOGO/1.svg"/>
</p><br/>

> Web Application

## Links

- 📘 [Documentation](https://goodsigns.ru/dev/docs)

## Getting started

```bash
# serve vuepress at  http://localhost:8080/docs
$ yarn docs:dev

# build vuepress to /docs
$ yarn docs:build
```

## Installation

```bash
$ yarn install
```

## Build

```bash
# cleaning up the project from the generated files
$ yarn clean

# the build analyze
$ yarn build:check

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Development

```bash
# install dependencies
$ yarn install

# running all tests
$ yarn test

# creating env files
$ yarn create-env

# running code analysis
$ yarn lint

# serve with hot reload at localhost:3000
$ yarn dev
```

&copy; 2020, [Dmitriy Maziuk](https://ma23.ru)
